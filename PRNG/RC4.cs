﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRNG
{
    class RC4 : RNG
    {
        private int[] S;
        private int[] key = new int[] { 517, 955, 558, 126, 584, 306, 465, 692, 666, 815, 198, 408, 719, 280, 144, 968 };
        private int i = 0;
        private int j = 0;
        private int m = 256;
        private int mod = 1001;

        public RC4()
        {
            S = new int[m];

            for (i = 0; i < m; i++)
            {
                S[i] = i;
            }

            j = 0;
            for (i = 0; i < m; i++)
            {
                j = (j + S[i] + key[i % key.Length]) % m;
                swap(S, i, j);
            }

            i = j = 0;
        }

        public RC4(int[] values)
        {            
            this.mod = values[0];

            S = new int[m];

            key = new int[values.Length - 1];
            for (int i = 1; i < values.Length; i++)
            {
                key[i - 1] = values[i];
            }

            for (i = 0; i < m; i++)
            {
                S[i] = i;
            }

            j = 0;
            for (i = 0; i < m; i++)
            {
                j = (j + S[i] + key[i % key.Length]) % m;
                swap(S, i, j);
            }

            i = j = 0;
        }

        private int getNexpPart()
        {
            i = (i + 1) % m;
            j = (j + S[i]) % m;
            swap(S, i, j);
            int K = S[(S[i] + S[j]) % m];
            return K;
        }

        public int getNext()
        {
            int a = getNexpPart();
            int b = getNexpPart();

            return ((a << 8) | b) % mod;
        }

        private void swap(int[] array, int idx1, int idx2)
        {
            int temp = array[idx1];
            array[idx1] = array[idx2];
            array[idx2] = temp;
        }
    }
}

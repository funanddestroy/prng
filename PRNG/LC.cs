﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRNG
{
    class LC : RNG
    {
        private int value = 135;
        private int a = 1103515245;
        private int c = 12345;
        private int m = 1001;

        public LC() {}

        public LC(int[] values)
        {
            this.a = values[0];
            this.c = values[1];
            this.m = values[2];
            this.value = (values[3] % m + m) % m;
        }

        public int getNext()
        {
            this.value = ((this.value * a + c) % m + m) % m;
            return this.value;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRNG
{
    class ADD : RNG
    {
        private List<int> values = new List<int>(new int[] { 140, 672, 387, 183, 808, 736, 499, 550, 139, 709, 631, 137, 163, 790, 732, 576, 642, 431, 26, 787, 122, 935, 701, 723, 101, 215, 120, 842, 415, 896, 494, 273, 788, 318, 797, 741, 249, 160, 805, 802, 580, 549, 820, 602, 773, 974, 322, 77, 103, 224, 624, 763, 714, 683, 204 });
        private int m = 1001;
        private int k = 24;
        private int j = 55;

        public ADD() { }

        public ADD(int[] values)
        {
            this.values = new List<int>(values);
            this.k = values[0];
            this.j = values[1];
            this.m = values[2];
        }

        public int getNext()
        {
            values.Add((values[values.Count - k] + values[values.Count - j]) % m);
            values.RemoveAt(0);
            return values.Last();
        }
    }
}

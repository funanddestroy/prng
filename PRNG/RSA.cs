﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;


namespace PRNG
{
    class RSA : RNG
    {
        private BigInteger p = 9859;
        private BigInteger q = 7603;
        private BigInteger n;
        private BigInteger f;
        private BigInteger e = 3;
        private BigInteger x;
        private BigInteger l = 10;

        private BigInteger getE(BigInteger f)
        {
            for (BigInteger i = 2; i < f; i++)
            {
                if (BigInteger.GreatestCommonDivisor(i, f) == 0)
                {
                    return i;
                }
            }

            return 13;
        }

        public RSA()
        {
            n = p * q;
            f = (p - 1) * (q - 1);

            e = getE(f);

            x = f / p;
        }

        public RSA(int[] values)
        {
            this.p = values[0];
            this.q = values[1];

            n = p * q;
            f = (p - 1) * (q - 1);

            e = getE(f);

            x = f / p;
        }

        public int getNext()
        {
            int z = 0;
            for (int i = 0; i < l; i++)
            {
                x = BigInteger.ModPow(x, e, n);
                z = (z << 1) | ((int)x & 1);
            }

            return z;
        }
    }
}

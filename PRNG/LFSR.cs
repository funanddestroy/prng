﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRNG
{
    class LFSR : RNG
    {
        private List<int> x = new List<int>(new int[] { 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0 });
        private int p = 20;
        private int m = 1001;
        private List<int> c = new List<int>(new int[] { 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0 });

        private int l = 10;

        public LFSR() { }

        public LFSR(int[] values)
        {
            this.p = values[0];
            this.m = values[1];

            c = new List<int>();
            for (int i = 2; i < p + 2; i++)
            {
                c.Add(values[i]);
            }

            this.x = new List<int>(values);
            for (int i = 0; i < p + 2; i++)
            {
                this.x.RemoveAt(0);
            }

            if (p < l)
            {
                l = p;
            }
        }

        public int getNext()
        {
            int tmp = 0;
            for (int i = p - 1; i > 0; i--)
            {
                tmp = (tmp + c[i] * x[i]) % 2;
            }
            tmp = (tmp + x[0]) % 2;

            c.Insert(0, tmp);
            c.RemoveAt(c.Count - 1);

            int ret = 0;

            for (int i = 0; i < l; i++)
            {
                ret += c[i] << i;
            }

            return ret % m;
        }
    }
}

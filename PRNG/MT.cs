﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRNG
{
    class MT : RNG
    {
        private List<int> values = new List<int>(new int[] { 517, 955, 558, 126, 584, 306, 465, 692, 666, 815, 198, 408, 719, 280, 144, 968, 793, 88, 639, 412 });
        private int p = 20;
        private int q = 10;
        private int r = 5;
        private int a = 867;
        private int w = 10;
        private int u;
        private int v;
        private int i = 0;

        public MT()
        {
            this.u = (1 << r) - 1;
            this.v = ((1 << w) - 1) ^ u;
        }

        public MT(int[] values)
        {
            this.values = new List<int>(values);

            this.p = values[0];
            this.q = values[1];
            this.r = values[2];
            this.a = values[3];
            this.w = values[4];

            this.u = (1 << r) - 1;
            this.v = ((1 << w) - 1) ^ u;

            for (int i = 0; i < 5; i++)
            {
                this.values.RemoveAt(0);
            }
        }

        public int getNext()
        {
            int x = values[i] & v | values[(i + 1) % p] & u;
            if ((x & 1) == 0)
            {
                values[i] = (values[(i + q) % p] ^ (x >> 1) ^ 0);
            }
            else
            {
                values[i] = (values[(i + q) % p] ^ (x >> 1) ^ a);
            }

            i = (i + 1) % p;

            return values[i];
        }
    }
}

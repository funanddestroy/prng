﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace PRNG
{
    class Program
    {
        static string help = "\t/g:<код_метода> - параметр указывает на метод генерации ПСЧ, при этом код_метода может быть одним из следующих:\n" +
                "\t\t * lc\t– линейный конгруэнтный метод;\n" +
                "\t\t * add\t– аддитивный метод;\n" +
                "\t\t * 5p\t– пятипараметрический метод;\n" +
                "\t\t * lfsr\t– регистр сдвига с обратной связью (РСЛОС);\n" +
                "\t\t * nfsr\t– нелинейная комбинация РСЛОС;\n" +
                "\t\t * mt\t– вихрь Мерсенна;\n" +
                "\t\t * rc4\t– RC4;\n" +
                "\t\t * rsa\t– ГПСЧ на основе RSA;\n" +
                "\t\t * bbs\t– алгоритм Блюма-Блюма - Шуба.\n\n" +
                "\t/i:<число> - инициализационный вектор генератора (разделение чисел производится символом \';\').\n\n" +
                "\t/n:<длина> - количество генерируемых чисел. Если параметр не указан, - генерируется 10000 чисел.\n\n" +
                "\t/f:<полное_имя_файла> - полное имя файла, в который будут выводиться данные. Если параметр не указан, данные должны записываться в файл с именем rnd.dat.\n";

        public struct Context
        {
            public Method g;
            public int[] i;
            public int n;
            public string f;
            public bool iFlag;
        }

        public enum Method
        {
            LC,
            ADD,
            FP,
            LFSR,
            NFSR,
            MT,
            RC4,
            RSA,
            BBS
        }

        public static Method getMethod(string s)
        {
            switch (s)
            {
                case "lc":
                    return Method.LC;
                case "add":
                    return Method.ADD;
                case "5p":
                    return Method.FP;
                case "lfsr":
                    return Method.LFSR;
                case "nfsr":
                    return Method.NFSR;
                case "mt":
                    return Method.MT;
                case "rc4":
                    return Method.RC4;
                case "rsa":
                    return Method.RSA;
                case "bbs":
                    return Method.BBS;
                default:
                    throw new ArgumentException();
            }
        }

        public static int[] getVector(string str)
        {
            return str.Split(new char[] { ';' })
                .Select(int.Parse)
                .ToArray<int>();
        }

        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("\t/h – информация о допустимых параметрах командной строки программы.\n");
                return;
            }

            if (args.Length == 1 && args[0] == "/h")
            {
                Console.WriteLine(help);
                return;
            }

            if (args.Length > 4)
            {
                throw new ArgumentException();
            }

            Context context = new Context();
            context.g = Method.LC;
            context.f = "rnd.dat";
            context.i = new int[] { 135 };
            context.n = 10000;
            context.iFlag = false;

            foreach (string s in args)
            {
                string[] _s = s.Split(new[] { ':' });

                if (_s.Length != 2)
                {
                    throw new ArgumentException();
                }

                if (_s[0] != "/g" &&
                     _s[0] != "/i" &&
                     _s[0] != "/f" &&
                     _s[0] != "/n")
                {
                    throw new ArgumentException();
                }

                switch (_s[0])
                {
                    case "/g":
                        context.g = getMethod(_s[1]);
                        break;
                    case "/i":
                        context.i = getVector(_s[1]);
                        context.iFlag = true;
                        break;
                    case "/f":
                        context.f = _s[1];
                        break;
                    case "/n":
                        context.n = int.Parse(_s[1]);
                        break;
                }
            }

            RNG rand = new LC();

            switch (context.g)
            {
                case Method.LC:
                    Console.WriteLine("Генерация чисел линейным конгруэнтным методом");
                    rand = context.iFlag? new LC(context.i) : new LC();
                    break;
                case Method.ADD:
                    Console.WriteLine("Генерация чисел аддитивным методом");
                    rand = context.iFlag ? new ADD(context.i) : new ADD();
                    break;
                case Method.FP:
                    Console.WriteLine("Генерация чисел пятипараметрическим методом");
                    rand = context.iFlag ? new FP(context.i) : new FP();
                    break;
                case Method.LFSR:
                    Console.WriteLine("Генерация чисел методом РЛОС");
                    rand = context.iFlag ? new LFSR(context.i) : new LFSR();
                    break;
                case Method.NFSR:
                    Console.WriteLine("Генерация чисел нелинейной комбинацией РЛОС");
                    rand = context.iFlag ? new NFSR(context.i) : new NFSR();
                    break;
                case Method.MT:
                    Console.WriteLine("Генерация чисел с помощью вихря Мерсенна");
                    rand = context.iFlag ? new MT(context.i) : new MT();
                    break;
                case Method.RC4:
                    Console.WriteLine("Генерация чисел с помощью алгоритма RC4");
                    rand = context.iFlag ? new RC4(context.i) : new RC4();
                    break;
                case Method.RSA:
                    Console.WriteLine("Генерация чисел с помощью алгоритма RSA");
                    rand = context.iFlag ? new RSA(context.i) : new RSA();
                    break;
                case Method.BBS:
                    Console.WriteLine("Генерация чисел с помощью алгоритма BBS");
                    rand = context.iFlag ? new BBS(context.i) : new BBS();
                    break;
            }

            StreamWriter sw = new StreamWriter(context.f);

            for (int i = 0; i < context.n; i++)
            {
                sw.Write(rand.getNext() + " ");
            }

            sw.Close();

            Console.WriteLine("Сгенерирована последовательность из " + context.n + " чисел в файл " + context.f);
        }
    }
}

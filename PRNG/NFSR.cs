﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRNG
{
    class NFSR : RNG
    {
        private RNG firstLFSR = new LFSR(new int[] { 19, 1001, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0 });
        private RNG secondLFSR = new LFSR(new int[] { 3, 1001, 1, 0, 1, 1, 0, 1 });
        private RNG thirdLFSR = new LFSR(new int[] { 55, 1001, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1 });

        public NFSR() { }

        public NFSR(int[] values)
        {
            int n1 = values[0] * 2 + 2;
            int n2 = values[n1] * 2 + 2 + n1;
            int n3 = values[n2] * 2 + 2 + n2;

            int[] val = new int[n1];
            for (int i = 0; i < n1; i++)
            {
                val[i] = values[i];
            }

            firstLFSR = new LFSR(val);

            val = new int[n2 - n1];
            for (int i = n1; i < n2; i++)
            {
                val[i - n1] = values[i];
            }

            secondLFSR = new LFSR(val);

            val = new int[n3 - n2];
            for (int i = n2; i < n3; i++)
            {
                val[i - n2] = values[i];
            }

            thirdLFSR = new LFSR(val);
        }

        public int getNext()
        {
            int x1 = firstLFSR.getNext();
            int x2 = secondLFSR.getNext();
            int x3 = thirdLFSR.getNext();

            int ret = ((x1 * x2) ^ (x2 ^ x3) ^ x3) & 1023;

            return ret;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRNG
{
    class FP : RNG
    {
        private List<int> values = new List<int>(new int[] { 790, 447, 806, 657, 870, 39, 335, 498, 565, 102, 224, 263, 451, 947, 199, 132, 740, 239, 577, 655, 199, 58, 661, 783, 678, 57, 59, 143, 902, 991, 14, 327, 988, 373, 585, 684, 557, 190, 805, 500, 505, 938, 186, 844, 407, 882, 651, 929, 378, 534, 468, 95, 35, 647, 13, 379, 433, 23, 626, 460, 627, 839, 243, 574, 88, 544, 922, 725, 9, 515, 575, 550, 81, 77, 797, 908, 872, 464, 23, 796, 406, 130, 765, 368, 170, 919, 292, 341, 360 });
        private int p = 89;
        private int q1 = 20;
        private int q2 = 40;
        private int q3 = 69;
        private int w = (1 << 10) - 1;

        public FP() { }

        public FP(int[] values)
        {            
            this.p = values[0];
            this.q1 = values[1];
            this.q2 = values[2];
            this.q3 = values[3];
            this.w = (1 << values[4]) - 1;

            this.values = new List<int>(values);
            for (int i = 0; i < 5; i++)
            {
                this.values.RemoveAt(0);
            }
        }

        public int getNext()
        {
            values.Add((values[q1] ^ values[q2] ^ values[q3] ^ values[0]) & w);
            values.RemoveAt(0);
            return values.Last();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Task2
{
    class NR : Distribution
    {
        private int a;
        private int b;
        private int[] array;
        private int max;

        public NR(string fileName, int a, int b)
        {
            string file = File.ReadAllText(fileName);
            array = file
                .Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse)
                .ToArray<int>();

            max = array[0];
            foreach (int arr in array)
            {
                if (arr > max)
                {
                    max = arr;
                }
            }

            this.a = a;
            this.b = b;
        }

        private double[] getU()
        {
            double[] U = new double[array.Length];
            for (int i = 0; i < array.Length; i++)
            {
                U[i] = (double)array[i] / (double)max;
            }
            return U;
        }

        public double[] getDistribution()
        {
            double[] U = getU();
            double[] Z = new double[U.Length];
            for (int i = 1; i < U.Length; i += 2)
            {
                Z[i - 1] = a + b * Math.Sqrt(-2 * Math.Log(U[i - 1])) * Math.Cos(2 * Math.PI * U[2]);
                Z[i] = a + b * Math.Sqrt(-2 * Math.Log(U[i - 1])) * Math.Sin(2 * Math.PI * U[2]);
            }
            return Z;
        }
    }
}

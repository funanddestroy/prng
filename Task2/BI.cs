﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Task2
{
    class BI : Distribution
    {
        private int[] array;
        private int max;
        private int n;
        private long[,] C;

        public BI(string fileName)
        {
            string file = File.ReadAllText(fileName);
            array = file
                .Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse)
                .ToArray<int>();

            max = array[0];
            foreach (int arr in array)
            {
                if (arr > max)
                {
                    max = arr;
                }
            }

            this.n = array.Length;

            this.C = new long[n + 1, n + 1];

            for (int i = 0; i < n + 1; i++)
            {
                for (int j = 0; j < n + 1; j++)
                {
                    this.C[i, j] = 0;
                }
            }

            for (int i = 0; i <= n; ++i)
            {
                C[i, 0] = 1;
                C[i, i] = 1;
                for (int j = 1; j < i; ++j)
                {
                    C[i, j] = C[i - 1, j - 1] + C[i - 1, j];
                }
            }
        }

        private double F(int y)
        {
            double ans = 0;
            double p = 1.0 / (double)n;
            for (int k = 1; k < y; k++)
            {
                ans += C[n, k] * Math.Pow(p, (double)k) * Math.Pow(1 - p, (double)(n - k));
            }

            return ans;
        }

        private double[] getU()
        {
            double[] U = new double[array.Length];
            for (int i = 0; i < array.Length; i++)
            {
                U[i] = (double)array[i] / (double)max;
            }
            return U;
        }

        public double[] getDistribution()
        {
            double[] U = getU();
            double[] Y = new double[U.Length];
            
            for (int i = 0; i < U.Length; i++)
            {
                int j = 0;
                for (; j <= n; j++)
                {
                    double fy = F(j);
                    if (U[i] <= fy)
                    {
                        break;
                    }
                }
                Y[i] = j;
            }

            return Y;
        }
    }
}

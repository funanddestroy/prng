﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Task2
{
    class Program
    {
        static string help = "\t/f:<имя_файла> - имя файла с входной последовательностью.\n\n" +
                "\t/d:<распределение> - код распределения для преобразования последовательности. Рекомендуется использовать следующие коды распределений:\n" +
                    "\t\t * st – стандартное равномерное с заданным интервалом;\n" +
                    "\t\t * tr – треугольное распределение;\n" +
                    "\t\t * ex – общее экспоненциальное распределение;\n" +
                    "\t\t * nr – нормальное распределение;\n" +
                    "\t\t * gm – гамма распределение;\n" +
                    "\t\t * ln – логнормальное распределение;\n" +
                    "\t\t * ls – логистическое распределение;\n" +
                    "\t\t * bi – биномиальное распределение.\n\n" +
                "\t/p1:<параметр1> - 1-й параметр, необходимый, для генерации ПСЧ заданного распределения.\n\n" +
                "\t/p2:<параметр1> - 2-й параметр, необходимый, для генерации ПСЧ заданного распределения.\n\n" +
                "\t/p3:<параметр2> - 3-й параметр, необходимый, для генерации ПСЧ заданного распределения.\n";

        public struct Context
        {
            public Distr d;
            public int p1;
            public int p2;
            public int p3;
            public string inputFile;
            public string outputFile;
            public bool iFlag;
        }

        public enum Distr
        {
            ST,
            TR,
            EX,
            NR,
            GM,
            LN,
            LS,
            BI
        }

        public static Distr getDistribution(string s)
        {
            switch (s)
            {
                case "st":
                    return Distr.ST;
                case "tr":
                    return Distr.TR;
                case "ex":
                    return Distr.EX;
                case "nr":
                    return Distr.NR;
                case "gm":
                    return Distr.GM;
                case "ln":
                    return Distr.LN;
                case "ls":
                    return Distr.LS;
                case "bi":
                    return Distr.BI;
                default:
                    throw new ArgumentException();
            }
        }

        public static int[] getVector(string str)
        {
            return str.Split(new char[] { ';' })
                .Select(int.Parse)
                .ToArray<int>();
        }

        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("\t/h – информация о допустимых параметрах командной строки программы.\n");
                return;
            }

            if (args.Length == 1 && args[0] == "/h")
            {
                Console.WriteLine(help);
                return;
            }

            if (args.Length > 6)
            {
                throw new ArgumentException();
            }

            Context context = new Context();
            context.d = Distr.ST;
            context.inputFile = "rnd.dat";
            context.outputFile = "out.dat";
            context.p1 = int.MinValue;
            context.p2 = int.MinValue;
            context.iFlag = false;

            foreach (string s in args)
            {
                string[] _s = s.Split(new[] { ':' });

                if (_s.Length != 2)
                {
                    throw new ArgumentException();
                }

                if (_s[0] != "/f" &&
                     _s[0] != "/d" &&
                     _s[0] != "/p1" &&
                     _s[0] != "/p2" &&
                     _s[0] != "/p3" &&
                     _s[0] != "/of")
                {
                    throw new ArgumentException();
                }

                switch (_s[0])
                {
                    case "/d":
                        context.d = getDistribution(_s[1]);
                        break;
                    case "/f":
                        context.inputFile = _s[1];
                        break;
                    case "/of":
                        context.outputFile = _s[1];
                        break;
                    case "/p1":
                        context.p1 = int.Parse(_s[1]);
                        break;
                    case "/p2":
                        context.p2 = int.Parse(_s[1]);
                        break;
                }
            }

            Distribution distribution = new ST(context.inputFile, context.p1, context.p2);
            Random random = new Random();

            switch (context.d)
            {
                case Distr.ST:
                    Console.WriteLine("Cтандартное равномерное с заданным интервалом");
                    if (context.p1 == int.MinValue)
                    {
                        context.p1 = random.Next() % 749 + 1;
                    }
                    if (context.p2 == int.MinValue)
                    {
                        context.p2 = random.Next() % (999 - context.p1) + 1;
                    }
                    distribution = new ST(context.inputFile, context.p1, context.p2);
                    break;
                case Distr.TR:
                    Console.WriteLine("Треугольное распределение");
                    if (context.p1 == int.MinValue)
                    {
                        context.p1 = random.Next() % 399 + 501;
                    }
                    if (context.p2 == int.MinValue)
                    {
                        context.p2 = random.Next() % (999 - context.p1) + 1;
                    }
                    distribution = new TR(context.inputFile, context.p1, context.p2);
                    break;
                case Distr.EX:
                    Console.WriteLine("Общее экспоненциальное распределение");
                    if (context.p1 == int.MinValue)
                    {
                        context.p1 = random.Next() % 39 + 51;
                    }
                    if (context.p2 == int.MinValue)
                    {
                        context.p2 = random.Next() % (99 - context.p1) + 1;
                    }
                    distribution = new EX(context.inputFile, context.p1, context.p2);
                    break;
                case Distr.NR:
                    Console.WriteLine("Нормальное распределение");
                    if (context.p1 == int.MinValue)
                    {
                        context.p1 = 0;
                    }
                    if (context.p2 == int.MinValue)
                    {
                        context.p2 = 1;
                    }
                    distribution = new NR(context.inputFile, context.p1, context.p2);
                    break;
                case Distr.GM:
                    Console.WriteLine("Гамма распределение");
                    if (context.p1 == int.MinValue)
                    {
                        context.p1 = random.Next() % 39 + 51;
                    }
                    if (context.p2 == int.MinValue)
                    {
                        context.p2 = random.Next() % (99 - context.p1) + 1;
                    }
                    distribution = new GM(context.inputFile, context.p1, context.p2);
                    break;
                case Distr.LN:
                    Console.WriteLine("Логнормальное распределение");
                    if (context.p1 == int.MinValue)
                    {
                        context.p1 = 3;
                    }
                    if (context.p2 == int.MinValue)
                    {
                        context.p2 = 1;
                    }
                    distribution = new LN(context.inputFile, context.p1, context.p2);
                    break;
                case Distr.LS:
                    Console.WriteLine("Логистическое распределение");
                    if (context.p1 == int.MinValue)
                    {
                        context.p1 = random.Next() % 39 + 51;
                    }
                    if (context.p2 == int.MinValue)
                    {
                        context.p2 = random.Next() % (99 - context.p1) + 1;
                    }
                    distribution = new LS(context.inputFile, context.p1, context.p2);
                    break;
                case Distr.BI:
                    Console.WriteLine("Биномиальное распределение");
                    distribution = new BI(context.inputFile);
                    break;
            }

            StreamWriter sw = new StreamWriter(context.outputFile);

            foreach(double d in distribution.getDistribution())
            {
                sw.Write(d + " ");
            }

            sw.Close();

            Console.WriteLine("Распределение записано в файл " + context.outputFile);

            
        }
    }
}
